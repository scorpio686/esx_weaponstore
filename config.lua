Config                  =   {}
Config.Debug            = true

Config.DrawDistance     =   100
Config.MarkerSize       =   {x = 1.1, y = 1.1, z = 0.9}
Config.MarkerType       =   29
Config.MarkerColor      =   {r = 102, g = 102, b = 204, a = 255}
Config.Locale           =   'fr'
Config.IllegalArmory    =   {
                                j = {
                                    Weapons = {vector3(2555.45, 4663.12, 33.07)}},
                            }
Config.IllegalNPC       =   {
                                {seller = true, model = "ig_cletus", x = 2555.45,  y = 4663.12,  z = 33.07, h = 200.0},
                                {seller = false, model = "csb_mweather", x = 2553.45,  y = 4665.32,  z = 33.08, h = 205.0},
                                {seller = false, model = "csb_mweather", x = 2557.54,  y = 4665.99,  z = 32.97, h = 140.0},
                            }
Config.LegalArmory      =   {
                                a = {Weapons = {vector3(-662.1, -935.3, 20.8)}},

                                b = {Weapons = {vector3(810.2, -2157.3, 28.6)}},

                                c = {Weapons = {vector3(1693.4, 3759.5, 33.7)}},

                                d = {Weapons = {vector3(-330.2, 6083.8, 30.4)}},

                                e = {Weapons = {vector3(252.3, -50.0, 68.9)}},

                                f = {Weapons = {vector3(22.0, -1107.2, 29.8)}},

                                g = {Weapons = {vector3(2567.6, 294.3, 107.7)}},

                                h = {Weapons = {vector3(-1117.5, 2698.6, 17.5)}},

                                i = {Weapons = {vector3(842.4, -1033.4, 27.1)}},

                            }

Config.Blip             =  {
                                Size  = 1.0,
                                Type  = 174,
                                Color = 25,
                                ShowBlip = true
                            }

Config.WeaponsTintsPrice_ClassicBlack       = 100
Config.WeaponsTintsPrice_ClassicGray        = 100
Config.WeaponsTintsPrice_ClassicTwoTone     = 100
Config.WeaponsTintsPrice_ClassicWhite       = 100
Config.WeaponsTintsPrice_ClassicBeige       = 100
Config.WeaponsTintsPrice_ClassicGreen       = 100
Config.WeaponsTintsPrice_ClassicBlue        = 100
Config.WeaponsTintsPrice_ClassicEarth       = 100
Config.WeaponsTintsPrice_ClassicBrownBlack  = 100
Config.WeaponsTintsPrice_RedContrast        = 100
Config.WeaponsTintsPrice_BlueContrast       = 100
Config.WeaponsTintsPrice_YellowContrast     = 100
Config.WeaponsTintsPrice_OrangeContrast     = 100
Config.WeaponsTintsPrice_BoldPink           = 100
Config.WeaponsTintsPrice_BoldPurpleYellow   = 100
Config.WeaponsTintsPrice_BoldOrange         = 100
Config.WeaponsTintsPrice_BoldGreenPurple    = 100
Config.WeaponsTintsPrice_BoldRedFeatures    = 100
Config.WeaponsTintsPrice_BoldGreenFeatures  = 100
Config.WeaponsTintsPrice_BoldCyanFeatures   = 100
Config.WeaponsTintsPrice_BoldYellowFeatures = 100
Config.WeaponsTintsPrice_BoldRedWhite       = 100
Config.WeaponsTintsPrice_BoldBlueWhite      = 100
Config.WeaponsTintsPrice_MetallicGold       = 100
Config.WeaponsTintsPrice_MetallicPlatinum   = 100
Config.WeaponsTintsPrice_MetallicGrayLilac  = 100
Config.WeaponsTintsPrice_MetallicPurpleLime = 100
Config.WeaponsTintsPrice_MetallicRed        = 100
Config.WeaponsTintsPrice_MetallicGreen      = 100
Config.WeaponsTintsPrice_MetallicBlue       = 100
Config.WeaponsTintsPrice_MetallicWhiteAqua  = 100
Config.WeaponsTintsPrice_MetallicRedYellow  = 100






    --Weapons Melee
Config.clip = 150
Config.weapon_dagger = 20
Config.weapon_bat = 10
Config.weapon_bottle = 30
Config.weapon_crowbar = 10
Config.weapon_flashlight = 15
Config.weapon_golfclub = 10
Config.weapon_hammer = 50
Config.weapon_hatchet = 75
Config.weapon_knuckle = 25
Config.weapon_knife = 40
Config.weapon_machete = 89
Config.weapon_switchblade = 190
Config.weapon_nightstick = 40
Config.weapon_wrench = 71
Config.weapon_battleaxe = 95
Config.weapon_poolcue = 62
Config.weapon_stone_hatchet = 75

    --Weapons Handguns
Config.weapon_pistol = 2500
Config.weapon_pistol_mk2 = 7375
Config.weapon_combatpistol = 3200
Config.weapon_appistol = 3200
Config.weapon_stungun = 100
Config.weapon_pistol50 = 3900
Config.weapon_snspistol = 2750
Config.weapon_snspistol_mk2 = 79575
Config.weapon_heavypistol = 3750
Config.weapon_vintagepistol = 3450
Config.weapon_flaregun = 3750
Config.weapon_marksmanpistol = 4350
Config.weapon_revolver = 5900
Config.weapon_revolver_mk2 = 99000
Config.weapon_doubleaction = 2000
Config.weapon_raypistol = 399000
Config.weapon_ceramicpistol = 20000
Config.weapon_navyrevolver = 2000
Config.weapon_gadgetpistol = 2000

    --Weapons Submachine Guns
Config.weapon_microsmg = 3750
Config.weapon_smg = 7500
Config.weapon_smg_mk2 = 85500
Config.weapon_assaultsmg = 12550
Config.weapon_combatpdw = 11750
Config.weapon_machinepistol = 6250
Config.weapon_minismg = 8900
Config.weapon_raycarbine = 449000

    --Weapons Shotguns
Config.weapon_pumpshotgun = 35000
Config.weapon_pumpshotgun_mk2 = 82500
Config.weapon_sawnoffshotgun = 300
Config.weapon_assaultshotgun = 10000
Config.weapon_bullpupshotgun = 8000
Config.weapon_musket = 2140
Config.weapon_heavyshotgun = 13550
Config.weapon_dbshotgun = 15450
Config.weapon_autoshotgun = 14900
Config.weapon_combatshotgun = 295000

    --Weapon Assault Rifles
Config.weapon_assaultrifle = 25000
Config.weapon_assaultrifle_mk2 = 28750
Config.weapon_carbinerifle = 20000
Config.weapon_carbinerifle_mk2 = 107500
Config.weapon_advancedrifle = 14250
Config.weapon_specialcarbine = 20000 -- Prix GTA Online 14750
Config.weapon_specialcarbine_mk2 = 135000
Config.weapon_bullpuprifle = 14500
Config.weapon_bullpuprifle_mk2 = 30575
Config.weapon_compactrifle = 14650
Config.weapon_militaryrifle = 397500

    --Weapon Light Machine Guns
Config.weapon_mg = 60000 -- Prix GTA online 13 500
Config.weapon_combatmg = 14800
Config.weapon_combatmg_mk2 = 79000
Config.weapon_gusenberg = 14600

    --Weapon Sniper Rifles
Config.weapon_sniperrifle = 90000 -- Prix GTA Online : 20 000
Config.weapon_heavysniper = 38150
Config.weapon_heavysniper_mk2 = 165375
Config.weapon_marksmanrifle = 15750
Config.weapon_marksmanrifle_mk2 = 149000

    --Weapon Heavy Weapons
Config.weapon_rpg = 26250
Config.weapon_grenadelauncher = 32400
Config.weapon_grenadelauncher_smoke = 100
Config.weapon_minigun = 50000
Config.weapon_firework = 65000
Config.weapon_railgun = 250000
Config.weapon_hominglauncher = 75000
Config.weapon_compactlauncher = 45000
Config.weapon_rayminigun = 499000

    --Weapon Throwables
Config.weapon_grenade = 250
Config.weapon_bzgas = 250
Config.weapon_molotov = 350
Config.weapon_stickybomb = 600
Config.weapon_proxmine = 600
Config.weapon_snowball = 5
Config.weapon_pipebomb = 500
Config.weapon_ball = 5
Config.weapon_smokegrenade = 150
Config.weapon_flare = 1250

    --Weapon Miscellaneous
Config.weapon_petrolcan = 100
Config.gadget_parachute = 100
Config.weapon_fireextinguisher = 100
Config.weapon_hazardcan = 100



-----------------------------------------------TEST

Config.Zones = {

    LTDgasoline = {
        Items = {
            {
                name = "clip",
                label = "bread",
                price = 150
            },
            {
                name = "weapon_dagger",
                label = "water",
                price = 20
            },
            {
                name = "weapon_bat",
                label = "water",
                price = 10
            },
            {
                name = "weapon_bottle",
                label = "water",
                price = 30
            },
            {
                name = "weapon_crowbar",
                label = "water",
                price = 10
            },
            {
                name = "weapon_flashlight",
                label = "water",
                price = 10
            },
            {
                name = "weapon_golfclub",
                label = "water",
                price = 10
            },
            {
                name = "weapon_hammer",
                label = "water",
                price = 50
            },
            {
                name = "weapon_hatchet",
                label = "water",
                price = 75
            },
            {
                name = "weapon_knuckle",
                label = "water",
                price = 25
            },
            {
                name = "weapon_knife",
                label = "water",
                price = 40
            },
            {
                name = "weapon_machete",
                label = "water",
                price = 89
            },
            {
                name = "weapon_switchblade",
                label = "water",
                price = 490
            },
            {
                name = "weapon_nightstick",
                label = "water",
                price = 40
            },
            {
                name = "weapon_wrench",
                label = "water",
                price = 71
            },
            {
                name = "weapon_battleaxe",
                label = "water",
                price = 95
            },
            {
                name = "weapon_poolcue",
                label = "water",
                price = 62
            },
            {
                name = "weapon_stone_hatchet",
                label = "water",
                price = 75
            }
        },
        Pos = {
            vector3(-48.5,  -1757.5, 28.4),
            vector3(1163.3, -323.8, 68.2),
            vector3(-707.5, -914.2, 18.2),
            vector3(-1820.5, 792.5, 137.1),
            vector3(1698.3, 4924.4, 41.0)
        },
        Size  = 1.0,
        Type  = 59,
        Color = 25,
        ShowBlip = true,
        ShowMarker = true
    }
}
