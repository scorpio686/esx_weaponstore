Locales['fr'] = {
-- Blips
  ['map_blip']            = 'Ammu-Nations',

-- Menu
  ['WeaponStoreMenu']     = 'Menu de l\'Ammu-Nation',
  ['press_menu']          = 'Appuez sur ~INPUT_CONTEXT~ pour accéder au menu',
  ['shop_item']           = '%s $',
  ['bought']              = 'vous venez d\'acheter ~y~%sx~s~ ~b~%s~s~ pour ~r~$%s~s~',
  ['not_enough']          = 'vous n\'avez ~r~pas assez~s~ d\'argent: %s',
  ['player_cannot_hold']  = 'vous n\'avez ~r~pas~s~ assez ~y~de place~s~ dans votre inventaire!',
  ['shop_confirm']        = 'acheter %sx %s pour $%s?',
  ['already_own']         = 'Vous possédez déjà cette arme',
  ['ammo_added']          = 'Munitions ajoutées',
  ['no']                  = 'non',
  ['yes']                 = 'oui',
  ['ppa_need']            = 'Vous n\'avez pas de PPA',
  ['PPA']        	        = 'License de port d\'armes',
  ['DeleteWeapons']       = 'Suppression des armes',
  ['WeaponsDeleted']      = '~r~Vos armes ont été retirées',


-- Catégories
  ['Melee']               = 'Armes de mêlée',
  ['Handguns']            = 'Armes de poing',
  ['SubmachineGuns']      = 'Pistolets-mitrailleurs',
  ['Shotguns']            = 'Fusils à pompe',
  ['AssaultRifles']       = 'Fusils d\'assaut',
  ['LightMachineGuns']    = 'Mitrailles légères',
  ['SniperRifles']        = 'Snipers',
  ['HeavyWeapons']        = 'Armes lourdes',
  ['Throwables']          = 'Jetables',
  ['Miscellaneous']       = 'Divers',
  ['WeaponsComponents']   = 'WeaponsComponents',
  ['WeaponsTints']        = 'WeaponsTints',


--Traduction des couleurs

  -- Traduction couleurs normales
  ['WeaponsTints_DefaultBlack']       = 'Skin Noir',
  ['WeaponsTints_Green']              = 'Skin Vert',
  ['WeaponsTints_Gold']               = 'Skin Doré',
  ['WeaponsTints_Pink']               = 'Skin Rose',
  ['WeaponsTints_Army']               = 'Skin Militaire',
  ['WeaponsTints_LSPD']               = 'Skin LSPD',
  ['WeaponsTints_Orange']             = 'Skin Orange',
  ['WeaponsTints_Platinum']           = 'Skin Platine',


  -- Traduction couleurs MK2
  ['WeaponsTints_ClassicBlack']       = 'Skin Noir',
  ['WeaponsTints_ClassicGray']        = 'Skin Gris',
  ['WeaponsTints_ClassicTwoTone']     = 'Skin Bicolore',
  ['WeaponsTints_ClassicWhite']       = 'Skin Blanc',
  ['WeaponsTints_ClassicBeige']       = 'Skin Beige',
  ['WeaponsTints_ClassicGreen']       = 'Skin Vert',
  ['WeaponsTints_ClassicBlue']        = 'Skin Bleu',
  ['WeaponsTints_ClassicEarth']       = 'Skin "Terre"',
  ['WeaponsTints_ClassicBrownBlack']  = 'Skin Noir et Brun',
  ['WeaponsTints_RedContrast']        = 'Skin Rouge',
  ['WeaponsTints_BlueContrast']       = 'Skin Bleu',
  ['WeaponsTints_YellowContrast']     = 'Skin Jaune',
  ['WeaponsTints_OrangeContrast']     = 'Skin Orange',
  ['WeaponsTints_BoldPink']           = 'Skin Rose',
  ['WeaponsTints_BoldPurpleYellow']   = 'Skin Violet et Jaune',
  ['WeaponsTints_BoldOrange']         = 'Skin Orange Audacieux',
  ['WeaponsTints_BoldGreenPurple']    = 'Skin Vert Audacieux',
  ['WeaponsTints_BoldRedFeatures']    = 'Skin Caractéristiques Rouge audacieux',
  ['WeaponsTints_BoldGreenFeatures']  = 'Skin Caractéristiques Vert audacieux',
  ['WeaponsTints_BoldCyanFeatures']   = 'Skin Caractéristiques Cyan audacieux',
  ['WeaponsTints_BoldYellowFeatures'] = 'Skin Caractéristiques Jaune audacieux',
  ['WeaponsTints_BoldRedWhite']       = 'Skin Rouge et blanc audacieux',
  ['WeaponsTints_BoldBlueWhite']      = 'Skin Bleu et blanc Audacieux',
  ['WeaponsTints_MetallicGold']       = 'Skin Or Métallique',
  ['WeaponsTints_MetallicPlatinum']   = 'Skin Platine Métallique',
  ['WeaponsTints_MetallicGrayLilac']  = 'Skin Lilas gris métallisé',
  ['WeaponsTints_MetallicPurpleLime'] = 'Skin Citron Vert Violet Métallisé',
  ['WeaponsTints_MetallicRed']        = 'Skin Rouge Métallisé',
  ['WeaponsTints_MetallicGreen']      = 'Skin Vert Métallisé',
  ['WeaponsTints_MetallicBlue']       = 'Skin Bleu Métallisé',
  ['WeaponsTints_MetallicWhiteAqua']  = 'Skin Aqua Blanc Métallisé',
  ['WeaponsTints_MetallicRedYellow']  = 'Skin Jaune Rouge Métallisé',


-- Traduction des armes

  -- Chargeur
  ['clip']                          = 'Chargeur',

  -- Arme de mêlée
  ['weapon_dagger']                 = 'Dague Bowie', -- https://en.wikipedia.org/wiki/Bowie_knife
  ['weapon_bat']                    = 'Batte',
  ['weapon_bottle']                 = 'Tesson de bouteille Stronzo',
  ['weapon_crowbar']                = 'Pied de biche',
  ['weapon_flashlight']             = 'Lampe de poche Kel-Lite',
  ['weapon_golfclub']               = 'Club de golf',
  ['weapon_hammer']                 = 'Marteau',
  ['weapon_hatchet']                = 'Hachette',
  ['weapon_knuckle']                = 'Poing américain',
  ['weapon_knife']                  = 'Coûteau UC-RB1',
  ['weapon_machete']                = 'Machette',
  ['weapon_switchblade']            = 'Couteau à cran d\'arrêt Italian Stiletto', -- https://en.wikipedia.org/wiki/Switchblade#Postwar_Sales_and_the_Italian_Stiletto
  ['weapon_nightstick']             = 'Matraque LSPD',
  ['weapon_wrench']                 = 'Clé à pipe',
  ['weapon_battleaxe']              = 'Hache',
  ['weapon_poolcue']                = 'Queue de billard',
  ['weapon_stone_hatchet']          = 'Hâche en pierre',

  -- Armes de poing
  ['weapon_pistol']                 = 'Pistolet .45 Taurus PT92AF', -- https://en.wikipedia.org/wiki/Taurus_PT92 https://gta.fandom.com/wiki/Pistol
  ['weapon_pistol_mk2']             = 'Pistolet .45 Beretta 92', -- https://en.wikipedia.org/wiki/Beretta_92 https://gta.fandom.com/wiki/Pistol_Mk_II
  ['weapon_combatpistol']           = 'Pistolet de combat .45 Heckler & Koch P2000', -- https://en.wikipedia.org/wiki/Heckler_%26_Koch_P2000 https://gta.fandom.com/wiki/Combat_Pistol
  ['weapon_appistol']               = 'Pistolet perforant .22 Colt SCAMP', -- https://en.wikipedia.org/wiki/Colt_SCAMP https://gta.fandom.com/wiki/Combat_Pistol
  ['weapon_stungun']                = 'Pistolet paralysant TASER 7', -- https://www.axon.com/products/taser-7 https://gta.fandom.com/wiki/Stun_Gun
  ['weapon_pistol50']               = 'Pistolet calibre .50', -- https://fr.wikipedia.org/wiki/FN_Five-seveN https://gta.fandom.com/wiki/Desert_Eagle
  ['weapon_snspistol']              = 'Pistolet SNS Heckler & Koch P7M10', -- https://en.wikipedia.org/wiki/Heckler_%26_Koch_P7#Variants https://gta.fandom.com/wiki/SNS_Pistol
  ['weapon_snspistol_mk2']          = 'Pistolet SNS AMT Backup', -- https://en.wikipedia.org/wiki/AMT_Backup https://gta.fandom.com/wiki/SNS_Pistol_Mk_II
  ['weapon_heavypistol']            = 'Pistolet lourd Entreprise Wide Body 1911', -- http://www.imfdb.org/wiki/File:Entreprise_Wide_Body_1911.jpg https://gta.fandom.com/wiki/Heavy_Pistol
  ['weapon_vintagepistol']          = 'Pistolet vintage FN Model 1922', -- https://fr.wikipedia.org/wiki/Browning_M1910 https://gta.fandom.com/wiki/Vintage_Pistol
  ['weapon_flaregun']               = 'Pistolet de détresse Orion', -- https://en.wikipedia.org/wiki/File:Single_shot,12_Gauge,_flare_gun.jpg https://gta.fandom.com/wiki/Flare_Gun
  ['weapon_marksmanpistol']         = 'Pistolet d\'élite Thompson/Center Contender G2', -- https://en.wikipedia.org/wiki/Thompson/Center_Contender https://gta.fandom.com/wiki/Marksman_Pistol
  ['weapon_revolver']               = 'Revolver Taurus Raging Bull', -- https://en.wikipedia.org/wiki/MP-412_REX https://gta.fandom.com/wiki/Heavy_Revolver 
  ['weapon_revolver_mk2']           = 'Revolver Chiappa Rhino', -- https://en.wikipedia.org/wiki/Chiappa_Rhino https://gta.fandom.com/wiki/Heavy_Revolver_Mk_II
  ['weapon_doubleaction']           = 'Revolver double action Colt M1892', -- https://gta.fandom.com/wiki/Double-Action_Revolver
  ['weapon_raypistol']              = 'Up-n-Atomizer', -- https://gta.fandom.com/wiki/Up-n-Atomizer
  ['weapon_ceramicpistol']          = 'Pistolet en céramique Heckler & Koch P7', -- https://en.wikipedia.org/wiki/Heckler_%26_Koch_P7 https://gta.fandom.com/wiki/Ceramic_Pistol
  ['weapon_navyrevolver']           = 'Revolver Colt 1851 Navy', -- http://en.wikipedia.org/wiki/Colt_1851_Navy_Revolver https://gta.fandom.com/wiki/Navy_Revolver
  ['weapon_gadgetpistol']           = 'Pistolet P08 Luger',  -- http://en.wikipedia.org/wiki/Luger_pistol https://gta.fandom.com/wiki/Perico_Pistol

  -- Pistolets-mitrailleurs
  ['weapon_microsmg']               = 'Mini Uzi 9mm',
  ['weapon_smg']                    = 'Pistolet-mitrailleur HK MP5A3', -- https://fr.wikipedia.org/wiki/HK_MP5 https://gta.fandom.com/wiki/MP5
  ['weapon_smg_mk2']                = 'Pistolet-mitrailleur SIG Sauer MPX',
  ['weapon_assaultsmg']             = 'Pistolet-mitrailleur FN P90 ',
  ['weapon_combatpdw']              = 'Pistolet-mitrailleur SIG MPX', -- http://en.wikipedia.org/wiki/SIG_MPX https://gta.fandom.com/wiki/Combat_PDW
  ['weapon_machinepistol']          = 'Pistolet-mitrailleur TEC-DC9', -- https://fr.wikipedia.org/wiki/TEC-9 https://gta.fandom.com/wiki/Tec-9
  ['weapon_minismg']                = 'Pistolet-mitrailleur Škorpion Vz. 61', -- https://en.wikipedia.org/wiki/%C5%A0korpion https://gta.fandom.com/wiki/Mini_SMG
  ['weapon_raycarbine']             = 'Mitraillette légère à plasma', --  https://gta.fandom.com/wiki/Unholy_Hellbringer

  -- Fusils à pompe
  ['weapon_pumpshotgun']            = 'Fusil à pompe Mossberg 590', -- https://fr.wikipedia.org/wiki/Mossberg_500 https://gta.fandom.com/wiki/Pump_Shotgun
  ['weapon_pumpshotgun_mk2']        = 'Fusil à pompe Remington 870', -- http://en.wikipedia.org/wiki/Remington_Model_870 https://gta.fandom.com/wiki/Pump_Shotgun_Mk_II
  ['weapon_sawnoffshotgun']         = 'Fusil à pompe Colt Model 1883 Hammerless', -- https://www.collectorebooks.com/gregg01/shotgun3/DSC02399.jpg https://gta.fandom.com/wiki/Sawed-Off_Shotgun
  ['weapon_assaultshotgun']         = 'Fusil à pompe UTAS UTS-15', -- https://en.wikipedia.org/wiki/UTAS_UTS-15 https://gta.fandom.com/wiki/Assault_Shotgun
  ['weapon_bullpupshotgun']         = 'Fusil à pompe Kel-Tec KSG', -- https://fr.wikipedia.org/wiki/Kel-Tec_KSG https://gta.fandom.com/wiki/Bullpup_Shotgun
  ['weapon_musket']                 = 'Mousquet Brown Bess', -- https://fr.wikipedia.org/wiki/Brown_Bess https://gta.fandom.com/wiki/Musket
  ['weapon_heavyshotgun']           = 'Fusil de chasse Saiga 12', -- https://fr.wikipedia.org/wiki/Saiga-12 https://gta.fandom.com/wiki/Heavy_Shotgun
  ['weapon_dbshotgun']              = 'Fusil de chasse à double canon Zabala', --  https://gta.fandom.com/wiki/Double_Barrel_Shotgun https://gta.fandom.com/wiki/Double_Barrel_Shotgun
  ['weapon_autoshotgun']            = 'Fusil de chasse automatique Sweeper Shotgun', -- https://en.wikipedia.org/wiki/Armsel_Striker https://gta.fandom.com/wiki/Sweeper_Shotgun
  ['weapon_combatshotgun']          = 'Fusil à pompe S.P.A.S. 12', -- https://fr.wikipedia.org/wiki/Franchi_SPAS_12 https://gta.fandom.com/wiki/Combat_Shotgun

  -- Fusils d\'assaut
  ['weapon_assaultrifle']           = 'Fusil d\'assaut AK-47 Norinco Type 56-2',  -- https://fr.wikipedia.org/wiki/Fusil_Type_56 https://gta.fandom.com/wiki/AK-47
  ['weapon_assaultrifle_mk2']       = 'Fusil d\'assaut AK-47 AKM', -- https://en.wikipedia.org/wiki/AKM https://gta.fandom.com/wiki/Assault_Rifle_Mk_II
  ['weapon_carbinerifle']           = 'Fusil d\'assaut AR-15s', -- https://en.wikipedia.org/wiki/AR-15_style_rifle https://gta.fandom.com/wiki/Carbine_Rifle
  ['weapon_carbinerifle_mk2']       = 'Fusil d\'assaut AR-15 SAI', -- https://en.wikipedia.org/wiki/AR-15_style_rifle https://gta.fandom.com/wiki/Carbine_Rifle_Mk_II
  ['weapon_advancedrifle']          = 'Fusil d\'assaut CTAR-21', -- https://iwi.net/iwi-tavor-rifle/tavor-ctar-integral/ https://gta.fandom.com/wiki/Advanced_Rifle
  ['weapon_specialcarbine']         = 'Fusil d\'assaut Heckler & Koch G36C', -- https://en.wikipedia.org/wiki/Heckler_%26_Koch_G36#Variants https://gta.fandom.com/wiki/Special_Carbine
  ['weapon_specialcarbine_mk2']     = 'Fusil d\'assaut & Koch G36K ', -- https://en.wikipedia.org/wiki/Heckler_%26_Koch_G36#Variants https://gta.fandom.com/wiki/Special_Carbine_Mk_II
  ['weapon_bullpuprifle']           = 'Fusil d\'assaut QBZ-95', -- https://fr.wikipedia.org/wiki/QBZ-95 https://gta.fandom.com/wiki/Bullpup_Rifle
  ['weapon_bullpuprifle_mk2']       = 'Fusil d\'assaut Kel-Tec RFB', -- https://en.wikipedia.org/wiki/Kel-Tec_RFB https://gta.fandom.com/wiki/Bullpup_Rifle_Mk_II
  ['weapon_compactrifle']           = 'Fusil d\'assaut Norinco Type 56', -- https://fr.wikipedia.org/wiki/Fusil_Type_56 https://gta.fandom.com/wiki/Compact_Rifle
  ['weapon_militaryrifle']          = 'Fusil d\'assaut Steyr AUG A3', -- https://fr.wikipedia.org/wiki/Steyr_AUG https://gta.fandom.com/wiki/Military_Rifle

  -- Mitrailles légères
  ['weapon_mg']                     = 'Mitrailleuse Kalachnikov PKM', -- https://fr.wikipedia.org/wiki/Mitrailleuse_Kalachnikov https://gta.fandom.com/wiki/MG
  ['weapon_combatmg']               = 'Mitrailleuse M249', -- https://en.wikipedia.org/wiki/Mk_48_machine_gun https://gta.fandom.com/wiki/Combat_MG
  ['weapon_combatmg_mk2']           = 'Mitrailleuse Mark 48', -- https://en.wikipedia.org/wiki/Mk_48_machine_gun https://gta.fandom.com/wiki/Combat_MG_Mk_II
  ['weapon_gusenberg']              = 'Mitrailleuse .45 ACP M1928A1', -- https://en.wikipedia.org/wiki/Thompson_submachine_gun https://gta.fandom.com/wiki/Gusenberg_Sweeper

  -- Sniper
  ['weapon_sniperrifle']            = 'Fusil de sniper Arctic Warfare AW-F', -- https://fr.wikipedia.org/wiki/Accuracy_International_AWP https://gta.fandom.com/wiki/Sniper_Rifle
  ['weapon_heavysniper']            = 'Fusil de sniper Barrett M107', -- https://en.wikipedia.org/wiki/Barrett_M82#M82_to_M107 https://gta.fandom.com/wiki/Heavy_Sniper
  ['weapon_heavysniper_mk2']        = 'Fusil de sniper Serbu BFG-50A', -- https://guns.fandom.com/wiki/Serbu_BFG-50A https://guns.fandom.com/wiki/Serbu_BFG-50A
  ['weapon_marksmanrifle']          = 'Fusil de sniper M39 EMR/Mk', -- https://en.wikipedia.org/wiki/M39_Enhanced_Marksman_Rifle https://gta.fandom.com/wiki/Marksman_Rifle
  ['weapon_marksmanrifle_mk2']      = 'Fusil de sniper Springfield M1A Loaded', -- https://gta.fandom.com/wiki/Marksman_Rifle https://gta.fandom.com/wiki/Marksman_Rifle_Mk_II

  -- Armes lourdes
  ['weapon_rpg']                    = 'Lance-roquettes RPG-7', -- https://fr.wikipedia.org/wiki/RPG-7 https://gta.fandom.com/wiki/Rocket_Launcher
  ['weapon_grenadelauncher']        = 'Lance-grenades Milkor MGL', -- https://en.wikipedia.org/wiki/Milkor_MGL https://gta.fandom.com/wiki/Grenade_Launcher
  ['weapon_grenadelauncher_smoke']  = 'Lance-grenades à fumée Milkor MGL', -- https://en.wikipedia.org/wiki/Milkor_MGL
  ['weapon_minigun']                = 'Minigun M134', -- https://fr.wikipedia.org/wiki/M134 https://gta.fandom.com/wiki/Minigun
  ['weapon_firework']               = 'Lanceur de feu d\'artifice', -- https://gta.fandom.com/wiki/Firework_Launcher
  ['weapon_railgun']                = 'Carabine à impulsion ARC-920', -- https://halo.fandom.com/wiki/Asymmetric_Recoilless_Carbine-920 https://gta.fandom.com/wiki/Railgun
  ['weapon_hominglauncher']         = 'Lance missile portatif SA-7 Grail', -- https://fr.wikipedia.org/wiki/9K32_Strela-2 https://gta.fandom.com/wiki/Homing_Launcher
  ['weapon_compactlauncher']        = 'Lance-grenades M79', -- https://fr.wikipedia.org/wiki/M134 https://gta.fandom.com/wiki/Compact_Grenade_Launcher
  ['weapon_rayminigun']             = 'Pistolet à plasma rotatif à 4 canons', -- https://gta.fandom.com/wiki/Widowmaker

  -- Jetables
  ['weapon_grenade']                = 'Grenade M61', -- https://en.wikipedia.org/wiki/M26_grenade²² https://gta.fandom.com/wiki/Grenades
  ['weapon_bzgas']                  = 'Grenade M18', -- https://en.wikipedia.org/wiki/M18_smoke_grenade https://gta.fandom.com/wiki/Grenades
  ['weapon_molotov']                = 'Cocktails Molotov Cherenkov Vodka', -- https://gta.fandom.com/wiki/Cherenkov_Vodka
  ['weapon_stickybomb']             = 'Bombe collante de C-4 M112', --  https://gta.fandom.com/wiki/Sticky_Bombs
  ['weapon_proxmine']               = 'Mines de proximité 01/B', -- https://gta.fandom.com/wiki/Proximity_Mines
  ['weapon_snowball']               = 'Boule de neige', -- https://gta.fandom.com/wiki/Snowballs
  ['weapon_pipebomb']               = 'Bombe artisanale', -- https://gta.fandom.com/wiki/Pipe_Bomb
  ['weapon_ball']                   = 'Balle de base-ball',
  ['weapon_smokegrenade']           = 'Grenade fumigène \'Smoke Red\'', -- https://en.wikipedia.org/wiki/M18_smoke_grenade http://www.imfdb.org/images/c/c7/M18red.jpg
  ['weapon_flare']                  = 'Fusée éclairante',

  -- Divers
  ['weapon_petrolcan']              = 'Jerrycan 20L',
  ['gadget_parachute']              = 'Parachute',
  ['weapon_fireextinguisher']       = 'Extincteur',
  ['weapon_hazardcan']              = 'Jerrican dangereux 20L',
}
