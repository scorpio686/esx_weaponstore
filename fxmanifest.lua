-- Resource Metadata
fx_version 'cerulean'
games { 'rdr3', 'gta5' }

author ''
description 'ESX Weapons Store'
version '1.0.0'

-- What to run
this_is_a_map 'yes'

client_scripts {
	'@es_extended/locale.lua',
	'config.lua',
	'locales/fr.lua',
	'client/esx_weaponstore_client.lua'
}

server_scripts {
	'@mysql-async/lib/MySQL.lua',
	'@es_extended/locale.lua',
	'config.lua',
	'locales/fr.lua',
	'server/esx_weaponstore_server.lua'
}

dependencies {
	'es_extended'
}
