local hasAlreadyEnteredMarker, lastZone
local currentAction, currentActionMsg, currentActionData = nil, nil, {}

ESX = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	ESX.PlayerData = ESX.GetPlayerData()
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  ESX.PlayerData = xPlayer
  ESX.PlayerLoaded = true
end)

RegisterNetEvent('esx:onPlayerLogout')
AddEventHandler('esx:onPlayerLogout', function()
  ESX.PlayerLoaded = false
  ESX.PlayerData = {}
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  ESX.PlayerData.job = job
end)

-- Create NPC Illegal
Citizen.CreateThread(function()
    while ESX == nil do
	TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
	Citizen.Wait(0)
    end
    for _, v in pairs(Config.IllegalNPC) do
        RequestModel(GetHashKey(v.model))
        while not HasModelLoaded(GetHashKey(v.model)) do
            Wait(1)
        end
        local npc = CreatePed(4, v.model, v.x, v.y, v.z, v.h,  false, true)
        SetPedFleeAttributes(npc, 0, 0)
        SetPedDropsWeaponsWhenDead(npc, false)
        SetPedDiesWhenInjured(npc, false)
        SetEntityInvincible(npc , true)
        FreezeEntityPosition(npc, true)
        SetBlockingOfNonTemporaryEvents(npc, true)
        if v.seller then 
            RequestAnimDict("missfbi_s4mop")
            while not HasAnimDictLoaded("missfbi_s4mop") do
                Wait(1)
            end
            TaskPlayAnim(npc, "missfbi_s4mop" ,"guard_idle_a" ,8.0, 1, -1, 49, 0, false, false, false)
        else
            GiveWeaponToPed(npc, GetHashKey("weapon_assaultrifle_mk2"), 2800, true, true)
        end
    end
end)

-- Create Blips
Citizen.CreateThread(function()
	for _,v in pairs(Config.LegalArmory) do
		for i = 1, #v.Weapons, 1 do
			--if Config.Blip.ShowBlip then
			local blip = AddBlipForCoord(v.Weapons[i])

			SetBlipSprite (blip, Config.Blip.Type)
			SetBlipScale  (blip, Config.Blip.Size)
			SetBlipColour (blip, Config.Blip.Color)
			SetBlipAsShortRange(blip, true)

			BeginTextCommandSetBlipName('STRING')
			AddTextComponentSubstringPlayerName(_U('map_blip'))
			EndTextCommandSetBlipName(blip)
			--end
		end
	end
end)

-- Key Controls
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)

		if currentAction then
			ESX.ShowHelpNotification(currentActionMsg)

			if IsControlJustReleased(0, 38) then
				if currentAction == 'shop_menu' then
					OpenWeaponStoreMenu(currentActionData.zone)
				end

				currentAction = nil
			end
		else
			Citizen.Wait(500)
		end
	end
end)

AddEventHandler('esx_weaponstore:hasEnteredMarker', function(zone)
	currentAction     = 'shop_menu'
	currentActionMsg  = _U('press_menu')
	currentActionData = {zone = zone}
end)

AddEventHandler('esx_weaponstore:hasExitedMarker', function()
	currentAction = nil
	ESX.UI.Menu.CloseAll()
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		local playerCoords = GetEntityCoords(PlayerPedId())
		local isInMarker, letSleep, currentZone = false, false

		for k,v in pairs(Config.IllegalArmory) do
			for i = 1, #v.Weapons, 1 do
				local distance = #(playerCoords - v.Weapons[i])

				if distance < Config.DrawDistance then

					DrawMarker(Config.MarkerType, v.Weapons[i], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, nil, nil, false)
				end
					letSleep = false

				if distance < 2.0 then
					isInMarker  = true
					currentZone = k
					lastZone    = k
				end
			end
		end

		for k,v in pairs(Config.LegalArmory) do
			for i = 1, #v.Weapons, 1 do
				local distance = #(playerCoords - v.Weapons[i])

				if distance < Config.DrawDistance then

					DrawMarker(Config.MarkerType, v.Weapons[i], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, nil, nil, false)
				end
				letSleep = false

				if distance < 2.0 then
					isInMarker  = true
					currentZone = k
					lastZone    = k
				end
			end
		end

		if isInMarker and not hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = true
			TriggerEvent('esx_weaponstore:hasEnteredMarker', currentZone)
		end

		if not isInMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			TriggerEvent('esx_weaponstore:hasExitedMarker', lastZone)
		end

		if letSleep then
			Citizen.Wait(500)
		end
	end
end)

function OpenWeaponStoreMenu()
	local PlayerPed = GetPlayerPed(-1)

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'WeaponStoreMenu', {
		title = _U('WeaponStoreMenu'),
		elements = {
			{label = _U('Melee'),				value = 'Melee'},
			{label = _U('Handguns'),			value = 'Handguns'},
			{label = _U('SubmachineGuns'),		value = 'SubmachineGuns'},
			{label = _U('Shotguns'),			value = 'Shotguns'},
			{label = _U('AssaultRifles'),		value = 'AssaultRifles'},
			{label = _U('LightMachineGuns'),	value = 'LightMachineGuns'},
			{label = _U('SniperRifles'),		value = 'SniperRifles'},
			{label = _U('HeavyWeapons'),		value = 'HeavyWeapons'},
			{label = _U('Throwables'),			value = 'Throwables'},
			{label = _U('Miscellaneous'),		value = 'Miscellaneous'},
			{label = '<span style="color:red;">--------------------------</span>'},
			{label = _U('WeaponsComponents'),	value = 'WeaponsComponents'},
			{label = _U('WeaponsTints'),		value = 'WeaponsTints'},
			{label = '<span style="color:red;">DEBUG :</span>' .. _U('DeleteWeapons'),		value = 'DeleteWeapons'}
		}

	}, function(data, menu)

		if data.current.value == 'DeleteWeapons' then

			DeleteWeapons()
			Citizen.Wait(500)
			OpenWeaponStoreMenu()
		end

		if data.current.value == 'Melee' then
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'Melee', {
				title = _U('Melee'),
				elements = {
					{label = _U('weapon_dagger')		.. ' - <span style="color:cyan;">' .. Config.weapon_dagger .. '</span> $',			value = 'weapon_dagger'},
					{label = _U('weapon_bat')			.. ' - <span style="color:cyan;">' .. Config.weapon_bat .. '</span> $',				value = 'weapon_bat'},
					{label = _U('weapon_bottle')		.. ' - <span style="color:cyan;">' .. Config.weapon_bottle .. '</span> $',			value = 'weapon_bottle'},
					{label = _U('weapon_crowbar')		.. ' - <span style="color:cyan;">' .. Config.weapon_crowbar .. '</span> $',			value = 'weapon_crowbar'},
					{label = _U('weapon_flashlight')	.. ' - <span style="color:cyan;">' .. Config.weapon_flashlight .. '</span> $',		value = 'weapon_flashlight'},
					{label = _U('weapon_golfclub')		.. ' - <span style="color:cyan;">' .. Config.weapon_golfclub .. '</span> $',		value = 'weapon_golfclub'},
					{label = _U('weapon_hammer')		.. ' - <span style="color:cyan;">' .. Config.weapon_hammer .. '</span> $',			value = 'weapon_hammer'},
					{label = _U('weapon_hatchet')		.. ' - <span style="color:cyan;">' .. Config.weapon_hatchet .. '</span> $',			value = 'weapon_hatchet'},
					{label = _U('weapon_knuckle')		.. ' - <span style="color:cyan;">' .. Config.weapon_knuckle .. '</span> $',			value = 'weapon_knuckle'},
					{label = _U('weapon_knife')			.. ' - <span style="color:cyan;">' .. Config.weapon_knife .. '</span> $',			value = 'weapon_knife'},
					{label = _U('weapon_machete')		.. ' - <span style="color:cyan;">' .. Config.weapon_machete .. '</span> $',			value = 'weapon_machete'},
					{label = _U('weapon_switchblade')	.. ' - <span style="color:cyan;">' .. Config.weapon_switchblade .. '</span> $',		value = 'weapon_switchblade'},
					{label = _U('weapon_nightstick')	.. ' - <span style="color:cyan;">' .. Config.weapon_nightstick .. '</span> $',		value = 'weapon_nightstick'},
					{label = _U('weapon_wrench')		.. ' - <span style="color:cyan;">' .. Config.weapon_wrench .. '</span> $',			value = 'weapon_wrench'},
					{label = _U('weapon_battleaxe')		.. ' - <span style="color:cyan;">' .. Config.weapon_battleaxe .. '</span> $',		value = 'weapon_battleaxe'},
					{label = _U('weapon_poolcue')		.. ' - <span style="color:cyan;">' .. Config.weapon_poolcue .. '</span> $',			value = 'weapon_poolcue'},
					{label = _U('weapon_stone_hatchet')	.. ' - <span style="color:cyan;">' .. Config.weapon_stone_hatchet .. '</span> $',	value = 'weapon_stone_hatchet'}
				}
			}, function(data2, menu2)
				--local player, distance = ESX.Game.GetClosestPlayer()
					local MaxAmmo = GetMaxAmmoInClip(PlayerPed, GetHashKey(data2.current.value), 1);

				--if distance ~= -1 and distance <= 3.0 then

					if data2.current.value == 'weapon_dagger' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_dagger)
					end

					if data2.current.value == 'weapon_bat' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_bat)
					end

					if data2.current.value == 'weapon_bottle' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_bottle)
					end

					if data2.current.value == 'weapon_crowbar' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_crowbar)
					end
					if data2.current.value == 'weapon_flashlight' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_flashlight)
					end

					if data2.current.value == 'weapon_golfclub' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_golfclub)
					end

					if data2.current.value == 'weapon_hammer' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_hammer)
					end

					if data2.current.value == 'weapon_hatchet' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_hatchet)
					end
					if data2.current.value == 'weapon_knuckle' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_knuckle)
					end

					if data2.current.value == 'weapon_knife' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_knife)
					end

					if data2.current.value == 'weapon_machete' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_machete)
					end

					if data2.current.value == 'weapon_switchblade' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_switchblade)
					end

					if data2.current.value == 'weapon_nightstick' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_nightstick)
					end
					if data2.current.value == 'weapon_wrench' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_wrench)
					end

					if data2.current.value == 'weapon_battleaxe' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_battleaxe)
					end

					if data2.current.value == 'weapon_poolcue' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_poolcue)
					end

					if data2.current.value == 'weapon_stone_hatchet' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_stone_hatchet)
					end
				--end
			end, function(data2, menu2)
				menu2.close()
			end)
		end

		if data.current.value == 'Handguns' then
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'Handguns', {
				title = _U('Handguns'),
				elements = {
					{label = _U('weapon_pistol')		.. ' - <span style="color:cyan;">' .. Config.weapon_pistol .. '</span> $',			value = 'weapon_pistol'},
					{label = _U('weapon_pistol_mk2')	.. ' - <span style="color:cyan;">' .. Config.weapon_pistol_mk2 .. '</span> $',		value = 'weapon_pistol_mk2'},
					{label = _U('weapon_combatpistol')	.. ' - <span style="color:cyan;">' .. Config.weapon_combatpistol .. '</span> $',	value = 'weapon_combatpistol'},
					{label = _U('weapon_appistol')		.. ' - <span style="color:cyan;">' .. Config.weapon_appistol .. '</span> $',		value = 'weapon_appistol'},
					{label = _U('weapon_stungun')		.. ' - <span style="color:cyan;">' .. Config.weapon_stungun .. '</span> $',			value = 'weapon_stungun'},
					{label = _U('weapon_pistol50')		.. ' - <span style="color:cyan;">' .. Config.weapon_pistol50 .. '</span> $',		value = 'weapon_pistol50'},
					{label = _U('weapon_snspistol')		.. ' - <span style="color:cyan;">' .. Config.weapon_snspistol .. '</span> $',		value = 'weapon_snspistol'},
					{label = _U('weapon_snspistol_mk2')	.. ' - <span style="color:cyan;">' .. Config.weapon_snspistol_mk2 .. '</span> $',	value = 'weapon_snspistol_mk2'},
					{label = _U('weapon_heavypistol')	.. ' - <span style="color:cyan;">' .. Config.weapon_heavypistol .. '</span> $',		value = 'weapon_heavypistol'},
					{label = _U('weapon_vintagepistol')	.. ' - <span style="color:cyan;">' .. Config.weapon_vintagepistol .. '</span> $',	value = 'weapon_vintagepistol'},
					{label = _U('weapon_flaregun')		.. ' - <span style="color:cyan;">' .. Config.weapon_flaregun .. '</span> $',		value = 'weapon_flaregun'},
					{label = _U('weapon_marksmanpistol').. ' - <span style="color:cyan;">' .. Config.weapon_marksmanpistol .. '</span> $',	value = 'weapon_marksmanpistol'},
					{label = _U('weapon_revolver')		.. ' - <span style="color:cyan;">' .. Config.weapon_revolver .. '</span> $',		value = 'weapon_revolver'},
					{label = _U('weapon_revolver_mk2')	.. ' - <span style="color:cyan;">' .. Config.weapon_revolver_mk2 .. '</span> $',	value = 'weapon_revolver_mk2'},
					{label = _U('weapon_doubleaction')	.. ' - <span style="color:cyan;">' .. Config.weapon_doubleaction .. '</span> $',	value = 'weapon_doubleaction'},
					{label = _U('weapon_raypistol')		.. ' - <span style="color:cyan;">' .. Config.weapon_raypistol .. '</span> $',		value = 'weapon_raypistol'},
					{label = _U('weapon_ceramicpistol')	.. ' - <span style="color:cyan;">' .. Config.weapon_ceramicpistol .. '</span> $',	value = 'weapon_ceramicpistol'},
					{label = _U('weapon_navyrevolver')	.. ' - <span style="color:cyan;">' .. Config.weapon_navyrevolver .. '</span> $',	value = 'weapon_navyrevolver'},
					{label = _U('weapon_gadgetpistol')	.. ' - <span style="color:cyan;">' .. Config.weapon_gadgetpistol .. '</span> $',	value = 'weapon_gadgetpistol'},
				}
			}, function(data2, menu2)
				--local player, distance = ESX.Game.GetClosestPlayer()
					local MaxAmmo = GetMaxAmmoInClip(PlayerPed, GetHashKey(data2.current.value), 1);

				--if distance ~= -1 and distance <= 3.0 then
					if data2.current.value == 'weapon_pistol' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_pistol)
					end

					if data2.current.value == 'weapon_pistol_mk2' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_pistol_mk2)
					end

					if data2.current.value == 'weapon_combatpistol' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_combatpistol)
					end

					if data2.current.value == 'weapon_appistol' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_appistol)
					end

					if data2.current.value == 'weapon_stungun' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_stungun)
					end

					if data2.current.value == 'weapon_pistol50' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_pistol50)
					end

					if data2.current.value == 'weapon_snspistol' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_snspistol)
					end

					if data2.current.value == 'weapon_snspistol_mk2' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_snspistol_mk2)
					end

					if data2.current.value == 'weapon_heavypistol' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_heavypistol)
					end

					if data2.current.value == 'weapon_vintagepistol' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_vintagepistol)
					end

					if data2.current.value == 'weapon_flaregun' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_flaregun)
					end

					if data2.current.value == 'weapon_marksmanpistol' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_marksmanpistol)
					end

					if data2.current.value == 'weapon_revolver' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_revolver)
					end

					if data2.current.value == 'weapon_doubleaction' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_doubleaction)
					end

					if data2.current.value == 'weapon_raypistol' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_raypistol)
					end

					if data2.current.value == 'weapon_ceramicpistol' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_ceramicpistol)
					end

					if data2.current.value == 'weapon_navyrevolver' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_navyrevolver)
					end

					if data2.current.value == 'weapon_gadgetpistol' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_gadgetpistol)
					end
				--end
			end, function(data2, menu2)
			menu2.close()
			end)
		end

		if data.current.value == 'SubmachineGuns' then
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'SubmachineGuns', {
				title = _U('SubmachineGuns'),
				elements = {
					{label = _U('weapon_microsmg')		.. ' - <span style="color:cyan;">' .. Config.weapon_microsmg .. '</span> $',		value = 'weapon_microsmg'},
					{label = _U('weapon_smg')			.. ' - <span style="color:cyan;">' .. Config.weapon_smg .. '</span> $',				value = 'weapon_smg'},
					{label = _U('weapon_smg_mk2')		.. ' - <span style="color:cyan;">' .. Config.weapon_smg_mk2 .. '</span> $',			value = 'weapon_smg_mk2'},
					{label = _U('weapon_assaultsmg')	.. ' - <span style="color:cyan;">' .. Config.weapon_assaultsmg .. '</span> $',		value = 'weapon_assaultsmg'},
					{label = _U('weapon_combatpdw')		.. ' - <span style="color:cyan;">' .. Config.weapon_combatpdw .. '</span> $',		value = 'weapon_combatpdw'},
					{label = _U('weapon_machinepistol')	.. ' - <span style="color:cyan;">' .. Config.weapon_machinepistol .. '</span> $',	value = 'weapon_machinepistol'},
					{label = _U('weapon_minismg')		.. ' - <span style="color:cyan;">' .. Config.weapon_minismg .. '</span> $',			value = 'weapon_minismg'},
					{label = _U('weapon_raycarbine')	.. ' - <span style="color:cyan;">' .. Config.weapon_raycarbine .. '</span> $',		value = 'weapon_raycarbine'}
				}
			}, function(data2, menu2)
				local MaxAmmo = GetMaxAmmoInClip(PlayerPed, GetHashKey(data2.current.value), 1);


					if data2.current.value == 'weapon_microsmg' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_microsmg)

					end

					if data2.current.value == 'weapon_smg' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_smg)
					end

					if data2.current.value == 'weapon_smg_mk2' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_smg_mk2)
					end

					if data2.current.value == 'weapon_assaultsmg' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_assaultsmg)
					end

					if data2.current.value == 'weapon_combatpdw' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_combatpdw)
					end

					if data2.current.value == 'weapon_machinepistol' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_machinepistol)
					end

					if data2.current.value == 'weapon_minismg' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_minismg)
					end

					if data2.current.value == 'weapon_raycarbine' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_raycarbine)
					end

			end, function(data2, menu2)
			menu2.close()
			end)
		end

		if data.current.value == 'Shotguns' then
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'Shotguns', {
				title = _U('Shotguns'),
				elements = {
					{label = _U('weapon_pumpshotgun')		.. ' - <span style="color:cyan;">' .. Config.weapon_pumpshotgun .. '</span> $',			value = 'weapon_pumpshotgun'},
					{label = _U('weapon_pumpshotgun_mk2')	.. ' - <span style="color:cyan;">' .. Config.weapon_pumpshotgun_mk2 .. '</span> $',		value = 'weapon_pumpshotgun_mk2'},
					{label = _U('weapon_sawnoffshotgun')	.. ' - <span style="color:cyan;">' .. Config.weapon_sawnoffshotgun .. '</span> $',		value = 'weapon_sawnoffshotgun'},
					{label = _U('weapon_assaultshotgun')	.. ' - <span style="color:cyan;">' .. Config.weapon_assaultshotgun .. '</span> $',		value = 'weapon_assaultshotgun'},
					{label = _U('weapon_bullpupshotgun')	.. ' - <span style="color:cyan;">' .. Config.weapon_bullpupshotgun .. '</span> $',		value = 'weapon_bullpupshotgun'},
					{label = _U('weapon_musket')			.. ' - <span style="color:cyan;">' .. Config.weapon_musket .. '</span> $',				value = 'weapon_musket'},
					{label = _U('weapon_heavyshotgun')		.. ' - <span style="color:cyan;">' .. Config.weapon_heavyshotgun .. '</span> $',		value = 'weapon_heavyshotgun'},
					{label = _U('weapon_dbshotgun')			.. ' - <span style="color:cyan;">' .. Config.weapon_dbshotgun .. '</span> $',			value = 'weapon_dbshotgun'},
					{label = _U('weapon_autoshotgun')		.. ' - <span style="color:cyan;">' .. Config.weapon_autoshotgun .. '</span> $',			value = 'weapon_autoshotgun'},
					{label = _U('weapon_combatshotgun')		.. ' - <span style="color:cyan;">' .. Config.weapon_combatshotgun .. '</span> $',		value = 'weapon_combatshotgun'}
				}
			}, function(data2, menu2)
				local MaxAmmo = GetMaxAmmoInClip(PlayerPed, GetHashKey(data2.current.value), 1);

					if data2.current.value == 'weapon_pumpshotgun' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_pumpshotgun)
					end

					if data2.current.value == 'weapon_pumpshotgun_mk2' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_pumpshotgun_mk2)
					end

					if data2.current.value == 'weapon_sawnoffshotgun' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_sawnoffshotgun)
					end

					if data2.current.value == 'weapon_assaultshotgun' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_assaultshotgun)
					end

					if data2.current.value == 'weapon_bullpupshotgun' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_bullpupshotgun)
					end

					if data2.current.value == 'weapon_musket' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_musket)
					end

					if data2.current.value == 'weapon_heavyshotgun' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_heavyshotgun)
					end

					if data2.current.value == 'weapon_dbshotgun' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_dbshotgun)
					end

					if data2.current.value == 'weapon_autoshotgun' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_autoshotgun)
					end

					if data2.current.value == 'weapon_combatshotgun' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_combatshotgun)
					end

			end, function(data2, menu2)
			menu2.close()
			end)
		end

		if data.current.value == 'AssaultRifles' then
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'Melee', {
				title = _U('Melee'),
				elements = {
					{label = _U('weapon_assaultrifle')		.. ' - <span style="color:cyan;">' .. Config.weapon_assaultrifle .. '</span> $',			value = 'weapon_assaultrifle'},
					{label = _U('weapon_assaultrifle_mk2')	.. ' - <span style="color:cyan;">' .. Config.weapon_assaultrifle_mk2 .. '</span> $',		value = 'weapon_assaultrifle_mk2'},
					{label = _U('weapon_carbinerifle')		.. ' - <span style="color:cyan;">' .. Config.weapon_carbinerifle .. '</span> $',			value = 'weapon_carbinerifle'},
					{label = _U('weapon_carbinerifle_mk2')	.. ' - <span style="color:cyan;">' .. Config.weapon_carbinerifle_mk2 .. '</span> $',		value = 'weapon_carbinerifle_mk2'},
					{label = _U('weapon_advancedrifle')		.. ' - <span style="color:cyan;">' .. Config.weapon_advancedrifle .. '</span> $',			value = 'weapon_advancedrifle'},
					{label = _U('weapon_specialcarbine')	.. ' - <span style="color:cyan;">' .. Config.weapon_specialcarbine .. '</span> $',			value = 'weapon_specialcarbine'},
					{label = _U('weapon_specialcarbine_mk2').. ' - <span style="color:cyan;">' .. Config.weapon_specialcarbine_mk2 .. '</span> $',		value = 'weapon_specialcarbine_mk2'},
					{label = _U('weapon_bullpuprifle')		.. ' - <span style="color:cyan;">' .. Config.weapon_bullpuprifle .. '</span> $',			value = 'weapon_bullpuprifle'},
					{label = _U('weapon_bullpuprifle_mk2')	.. ' - <span style="color:cyan;">' .. Config.weapon_bullpuprifle_mk2 .. '</span> $',		value = 'weapon_bullpuprifle_mk2'},
					{label = _U('weapon_compactrifle')		.. ' - <span style="color:cyan;">' .. Config.weapon_compactrifle .. '</span> $',			value = 'weapon_compactrifle'},
					{label = _U('weapon_militaryrifle')		.. ' - <span style="color:cyan;">' .. Config.weapon_militaryrifle .. '</span> $',			value = 'weapon_militaryrifle'}
				}
			}, function(data2, menu2)
				local MaxAmmo = GetMaxAmmoInClip(PlayerPed, GetHashKey(data2.current.value), 1);

					if data2.current.value == 'weapon_assaultrifle' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_assaultrifle)
					end

					if data2.current.value == 'weapon_assaultrifle_mk2' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_assaultrifle_mk2)
					end

					if data2.current.value == 'weapon_carbinerifle' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_carbinerifle)
					end

					if data2.current.value == 'weapon_carbinerifle_mk2' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_carbinerifle_mk2)
					end
					if data2.current.value == 'weapon_advancedrifle' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_advancedrifle)
					end

					if data2.current.value == 'weapon_specialcarbine' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_specialcarbine)
					end

					if data2.current.value == 'weapon_specialcarbine_mk2' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_specialcarbine_mk2)
					end

					if data2.current.value == 'weapon_bullpuprifle' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_bullpuprifle)
					end
					if data2.current.value == 'weapon_bullpuprifle_mk2' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_bullpuprifle_mk2)
					end

					if data2.current.value == 'weapon_compactrifle' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_compactrifle)
					end

					if data2.current.value == 'weapon_militaryrifle' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_militaryrifle)
					end

			end, function(data2, menu2)
			menu2.close()
			end)
		end

		if data.current.value == 'LightMachineGuns' then
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'LightMachineGuns', {
				title = _U('LightMachineGuns'),
				elements = {
					{label = _U('weapon_mg')			.. ' - <span style="color:cyan;">' .. Config.weapon_mg .. '</span> $',				value = 'weapon_mg'},
					{label = _U('weapon_combatmg')		.. ' - <span style="color:cyan;">' .. Config.weapon_combatmg .. '</span> $',		value = 'weapon_combatmg'},
					{label = _U('weapon_combatmg_mk2')	.. ' - <span style="color:cyan;">' .. Config.weapon_combatmg_mk2 .. '</span> $',	value = 'weapon_combatmg_mk2'},
					{label = _U('weapon_gusenberg')		.. ' - <span style="color:cyan;">' .. Config.weapon_gusenberg .. '</span> $',		value = 'weapon_gusenberg'}
				}
			}, function(data2, menu2)
				local MaxAmmo = GetMaxAmmoInClip(PlayerPed, GetHashKey(data2.current.value), 1);

					if data2.current.value == 'weapon_mg' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_mg)
					end

					if data2.current.value == 'weapon_combatmg' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_combatmg)
					end

					if data2.current.value == 'weapon_combatmg_mk2' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_combatmg_mk2)
					end

					if data2.current.value == 'weapon_gusenberg' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_gusenberg)
					end

			end, function(data2, menu2)
			menu2.close()
			end)
		end

		if data.current.value == 'SniperRifles' then
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'Melee', {
				title = _U('Melee'),
				elements = {
					{label = _U('weapon_sniperrifle')			.. ' - <span style="color:cyan;">' .. Config.weapon_sniperrifle .. '</span> $',			value = 'weapon_sniperrifle'},
					{label = _U('weapon_heavysniper')			.. ' - <span style="color:cyan;">' .. Config.weapon_heavysniper .. '</span> $',			value = 'weapon_heavysniper'},
					{label = _U('weapon_heavysniper_mk2')		.. ' - <span style="color:cyan;">' .. Config.weapon_heavysniper_mk2 .. '</span> $',		value = 'weapon_heavysniper_mk2'},
					{label = _U('weapon_marksmanrifle')			.. ' - <span style="color:cyan;">' .. Config.weapon_marksmanrifle .. '</span> $',		value = 'weapon_marksmanrifle'},
					{label = _U('weapon_marksmanrifle_mk2')		.. ' - <span style="color:cyan;">' .. Config.weapon_marksmanrifle_mk2 .. '</span> $',	value = 'weapon_marksmanrifle_mk2'}
				}
			}, function(data2, menu2)
				local MaxAmmo = GetMaxAmmoInClip(PlayerPed, GetHashKey(data2.current.value), 1);


					if data2.current.value == 'weapon_sniperrifle' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_sniperrifle)
					end

					if data2.current.value == 'weapon_heavysniper' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_heavysniper)
					end

					if data2.current.value == 'weapon_heavysniper_mk2' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_heavysniper_mk2)
					end

					if data2.current.value == 'weapon_marksmanrifle' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_marksmanrifle)
					end

					if data2.current.value == 'weapon_marksmanrifle_mk2' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_marksmanrifle_mk2)
					end

			end, function(data2, menu2)
			menu2.close()
			end)
		end

		if data.current.value == 'HeavyWeapons' then
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'HeavyWeapons', {
				title = _U('HeavyWeapons'),
				elements = {
					{label = _U('weapon_rpg')						.. ' - <span style="color:cyan;">' .. Config.weapon_rpg						.. '</span> $',	value = 'weapon_rpg'},
					{label = _U('weapon_grenadelauncher')			.. ' - <span style="color:cyan;">' .. Config.weapon_grenadelauncher			.. '</span> $',	value = 'weapon_grenadelauncher'},
					{label = _U('weapon_grenadelauncher_smoke')		.. ' - <span style="color:cyan;">' .. Config.weapon_grenadelauncher_smoke	.. '</span> $',	value = 'weapon_grenadelauncher_smoke'},
					{label = _U('weapon_minigun')					.. ' - <span style="color:cyan;">' .. Config.weapon_minigun					.. '</span> $',	value = 'weapon_minigun'},
					{label = _U('weapon_firework')					.. ' - <span style="color:cyan;">' .. Config.weapon_firework				.. '</span> $',	value = 'weapon_firework'},
					{label = _U('weapon_railgun')					.. ' - <span style="color:cyan;">' .. Config.weapon_railgun					.. '</span> $',	value = 'weapon_railgun'},
					{label = _U('weapon_hominglauncher')			.. ' - <span style="color:cyan;">' .. Config.weapon_hominglauncher			.. '</span> $',	value = 'weapon_hominglauncher'},
					{label = _U('weapon_compactlauncher')			.. ' - <span style="color:cyan;">' .. Config.weapon_compactlauncher			.. '</span> $',	value = 'weapon_compactlauncher'},
					{label = _U('weapon_rayminigun')				.. ' - <span style="color:cyan;">' .. Config.weapon_rayminigun				.. '</span> $',	value = 'weapon_rayminigun'}
				}
			}, function(data2, menu2)
				local MaxAmmo = GetMaxAmmoInClip(PlayerPed, GetHashKey(data2.current.value), 1);

					if data2.current.value == 'weapon_rpg' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_rpg)
					end

					if data2.current.value == 'weapon_grenadelauncher' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_grenadelauncher)
					end

					if data2.current.value == 'weapon_grenadelauncher_smoke' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_grenadelauncher_smoke)
					end

					if data2.current.value == 'weapon_minigun' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_minigun)
					end

					if data2.current.value == 'weapon_firework' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_firework)
					end

					if data2.current.value == 'weapon_railgun' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_railgun)
					end

					if data2.current.value == 'weapon_hominglauncher' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_hominglauncher)
					end

					if data2.current.value == 'weapon_compactlauncher' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_compactlauncher)
					end

					if data2.current.value == 'weapon_rayminigun' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_rayminigun)
					end

			end, function(data2, menu2)
			menu2.close()
			end)
		end

		if data.current.value == 'Throwables' then
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'Throwables', {
				title = _U('Throwables'),
				elements = {
					{label = _U('weapon_grenade')		.. ' - <span style="color:cyan;">' .. Config.weapon_grenade .. '</span> $',			value = 'weapon_grenade'},
					{label = _U('weapon_bzgas')			.. ' - <span style="color:cyan;">' .. Config.weapon_bzgas .. '</span> $',			value = 'weapon_bzgas'},
					{label = _U('weapon_molotov')		.. ' - <span style="color:cyan;">' .. Config.weapon_molotov .. '</span> $',			value = 'weapon_molotov'},
					{label = _U('weapon_stickybomb')	.. ' - <span style="color:cyan;">' .. Config.weapon_stickybomb .. '</span> $',		value = 'weapon_stickybomb'},
					{label = _U('weapon_proxmine')		.. ' - <span style="color:cyan;">' .. Config.weapon_proxmine .. '</span> $',		value = 'weapon_proxmine'},
					{label = _U('weapon_snowball')		.. ' - <span style="color:cyan;">' .. Config.weapon_snowball .. '</span> $',		value = 'weapon_snowball'},
					{label = _U('weapon_pipebomb')		.. ' - <span style="color:cyan;">' .. Config.weapon_pipebomb .. '</span> $',		value = 'weapon_pipebomb'},
					{label = _U('weapon_ball')			.. ' - <span style="color:cyan;">' .. Config.weapon_ball .. '</span> $',			value = 'weapon_ball'},
					{label = _U('weapon_smokegrenade')	.. ' - <span style="color:cyan;">' .. Config.weapon_smokegrenade .. '</span> $',	value = 'weapon_smokegrenade'},
					{label = _U('weapon_flare')			.. ' - <span style="color:cyan;">' .. Config.weapon_flare .. '</span> $',			value = 'weapon_flare'}
				}
			}, function(data2, menu2)
				local MaxAmmo = GetMaxAmmoInClip(PlayerPed, GetHashKey(data2.current.value), 1);

					if data2.current.value == 'weapon_grenade' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_grenade)
					end

					if data2.current.value == 'weapon_bzgas' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_bzgas)
					end

					if data2.current.value == 'weapon_molotov' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_molotov)
					end

					if data2.current.value == 'weapon_stickybomb' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_stickybomb)
					end
					if data2.current.value == 'weapon_proxmine' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_proxmine)
					end

					if data2.current.value == 'weapon_snowball' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_snowball)
					end

					if data2.current.value == 'weapon_pipebomb' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_pipebomb)
					end

					if data2.current.value == 'weapon_ball' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_ball)
					end
					if data2.current.value == 'weapon_smokegrenade' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_smokegrenade)
					end

					if data2.current.value == 'weapon_flare' then
						TriggerServerEvent('esx_weaponstore:buyItem', data2.current.value, MaxAmmo, Config.weapon_flare)
					end

			end, function(data2, menu2)
			menu2.close()
			end)
		end

		if data.current.value == 'Miscellaneous' then
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'Miscellaneous', {
				title = _U('Miscellaneous'),
				elements = {
					{label = _U('weapon_petrolcan')			.. ' - <span style="color:cyan;">' .. Config.weapon_petrolcan .. '</span> $',			value = 'weapon_petrolcan'},
					{label = _U('gadget_parachute')			.. ' - <span style="color:cyan;">' .. Config.weapon_parachute .. '</span> $',			value = 'weapon_parachute'},
					{label = _U('weapon_fireextinguisher')	.. ' - <span style="color:cyan;">' .. Config.weapon_fireextinguisher .. '</span> $',	value = 'weapon_fireextinguisher'},
					{label = _U('weapon_hazardcan')			.. ' - <span style="color:cyan;">' .. Config.weapon_hazardcan .. '</span> $',			value = 'weapon_hazardcan'}
				}
			}, function(data2, menu2)
				ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'shop_confirm', {
					title    = _U('shop_confirm', data.current.value, data.current.itemLabel, ESX.Math.GroupDigits(data.current.price * data.current.value)),
					align    = 'bottom-right',
					elements = {
						{label = _U('no'),  value = 'no'},
						{label = _U('yes'), value = 'yes'}
					}}, function(data3, menu3)
					if data2.current.value == 'yes' then
						TriggerServerEvent('esx_shops:buyItem', data.current.item, data.current.value, zone)
					end

					menu2.close()
				end, function(data3, menu3)
					menu2.close()
				end)
			end, function(data2, menu2)
				menu2.close()
			end)
		end

		--if data.current.value == 'WeaponsTints' then

		--	OpenWeaponMenu(shopData)

		--end
	end,function(data, menu)
		menu.close()

		currentAction     = 'shop_menu'
		currentActionMsg  = _U('press_menu')
		currentActionData = {zone = zone}
	end)
end


function DeleteWeapons()

	RemoveAllPedWeapons(PlayerPedId())
	ESX.ShowAdvancedNotification('DEBUG', _U('DeleteWeapons'), _U('WeaponsDeleted'), 'CHAR_MINOTAUR', 7)

	ESX.UI.Menu.CloseAll()
end


--[[
RegisterNetEvent('esx_weaponstore:weapon')
AddEventHandler('esx_weaponstore:weapon', function(weapon_name)
	local playerPed		= GetPlayerPed(-1)
	local MaxAmmo		= GetMaxAmmoInClip(playerPed, GetHashKey(weapon_name), 1)
	
	TriggerEvent('aquiverNotify', _U('ammo_added'), _U('clip'), 'clip.png', 8, 'rgba(255,0,0,0.25)')
	TriggerServerEvent('esx_weaponstore:weaponadd', weapon_name, MaxAmmo)

end)
]]
RegisterNetEvent('esx_weaponstore:ammoCount')
AddEventHandler('esx_weaponstore:ammoCount', function()

  local playerPed	= GetPlayerPed(-1)
  local WeaponHash	= GetSelectedPedWeapon(playerPed)
  local MaxAmmo		= GetMaxAmmoInClip(playerPed, WeaponHash, 1)

	--AddAmmoToPed(playerPed, WeaponHash, MaxAmmo)
	--UpdateAmmo()
	
	--SetAmmoInClip(playerPed, WeaponHash, MaxAmmo)
 -- AddAmmoToPed(GetPlayerPed(-1), WeaponHash,30)
  TriggerServerEvent('esx_weaponstore:Ammo', WeaponHash, MaxAmmo)
  
end)

function UpdateAmmo()	
	TriggerEvent('ammo:updateammo')	
	print('update ammo client weapon shop')
end


RegisterNetEvent('esx_weaponstore:tints')
AddEventHandler('esx_weaponstore:tints', function(values)
	local playerPed		=	GetPlayerPed(-1)
	local weapon_name	=	GetSelectedPedWeapon(playerPed)

	ESX.ShowNotification('values' .. values)
	if HasPedGotWeapon(GetPlayerPed(-1),GetHashKey(weapon_name),false) then -- Test if player have the Weapon
		SetPedWeaponTintIndex(playerPed, weapon_name, values)
	end
end)



---------------------------------------------------------------------------------------------------------------
