ESX = nil
local ShopItems = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

 ESX.RegisterServerCallback('esx_weaponstore:getItemAmount', function(source, cb, item)
 
	local xPlayer = ESX.GetPlayerFromId(source)
        local items = xPlayer.getInventoryItem(item)

        if items == nil then
            cb(0)
        else
            cb(items.count)
        end
end)

-- TriggerServerEvent('esx_weaponstore:buyItem', data.current.value, MaxAmmo, 2500)
RegisterServerEvent('esx_weaponstore:buyItem')
AddEventHandler('esx_weaponstore:buyItem', function(Weapon, MaxAmmo, price)
	local _source		= source
	local xPlayer		= ESX.GetPlayerFromId(_source)
	--local weaponLabel	= ESX.GetWeaponLabel(Weapon)

	if xPlayer.getMoney() >= price then
		print(xPlayer.getWeight())
		--if xPlayer.canCarryItem(Weapon, 1) then
			print("Yes tu peux prendre")
			if MaxAmmo == 0 then

				xPlayer.removeMoney(price)
				xPlayer.addWeapon(Weapon, MaxAmmo)
				xPlayer.showNotification('~o~Achat ~s~d\'un(e) ~o~' .. _U(Weapon) .. ' ~s~pour ~o~' .. price .. '$')

			else
				xPlayer.removeMoney(price)
				xPlayer.addWeapon(Weapon, MaxAmmo)
				xPlayer.showNotification('~o~Achat ~s~d\'un(e) ~o~' .. _U(Weapon) .. '~s~ avec ~o~' .. MaxAmmo .. ' ~o~cartouches ~s~pour ~o~' .. price .. '$')
			end
		--else
			print("Tu ne peux pas prendre")
    		xPlayer.showNotification(_U('already_own'))
		--end

	else
		local missingMoney = price - xPlayer.getMoney()

		xPlayer.showNotification(_U('not_enough', ESX.Math.GroupDigits(missingMoney)))

	end
end)

RegisterServerEvent('esx_weaponstore:Ammo')
AddEventHandler('esx_weaponstore:Ammo', function(weapon_name, MaxAmmo)
	local _source	= source
	local xPlayer	= ESX.GetPlayerFromId(_source)
	
	TriggerEvent('aquiverNotify', source, _U('ammo_added'), _U('clip'), 'clip.png', 8, 'rgba(255,0,0,0.25)')

end)

ESX.RegisterUsableItem('clip', function(source)
	local xPlayer 	= ESX.GetPlayerFromId(source)

	TriggerClientEvent('esx_weaponstore:ammoCount', source)
	xPlayer.removeInventoryItem("clip", 1)
	TriggerEvent('aquiverNotify', source, _U('ammo_added'), _U('clip'), 'clip.png', 8, 'rgba(255,0,0,0.25)')
end)
